import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "pystax",
    version = "0.0.1",
    author = "Johan Nestaas",
    author_email = "johannestaas@gmail.com",
    description = "astrophotography stacker",
    license = "GPLv3+",
    keywords = "imaging scientific astronomy photography astrophotography stacker",
    url = "https://bitbucket.org/johannestaas/pystax",
    packages=['pystax'],
    long_description=read('README.md'),
    classifiers=[
        #'Development Status :: 1 - Planning',
        #'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',
        #'Development Status :: 6 - Mature',
        #'Development Status :: 7 - Inactive',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Intended Audience :: Science/Research',
        'Environment :: Console',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
        'Topic :: Utilities',
        'Topic :: Scientific/Engineering :: Astronomy',
    ],
    install_requires=[
    ],
)
